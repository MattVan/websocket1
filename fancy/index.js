// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

server.listen(port, () => {
  console.log('Server listening at port %d', port);
});

// Routing
app.use(express.static(path.join(__dirname, 'public')));

// Chatroom

var numUsers = 0;
var genUsers = 0;
var schoolUsers = 0;

io.set('transports', ['websocket']);

io.on('connection', (socket) => {
  var addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', (data) => {
    // we tell the client to execute 'new message'
    io.to(socket.room).emit('new message', {
      username: socket.username, 
      message:data
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', (username, room) => {
    if (addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    socket.room = room;
    addedUser = true;
    socket.join(room);
    if(room == "general"){
      genUsers++;
      numUsers = genUsers;
    } else if(room == "school"){
      schoolUsers++;
      numUsers = schoolUsers;
    }
    
    socket.emit('login', {
      numUsers: numUsers
    });
    
    io.to(socket.room).emit('user joined', {
      username: socket.username, 
      numUsers: numUsers
    });
    
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', () => {
    io.to(socket.room).emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', () => {
    io.to(socket.room).emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', () => {
    if (addedUser) {
      // echo globally that this client has left
      
      socket.leave(socket.room);
      
      if(socket.room == "general"){
        genUsers--;
        numUsers = genUsers;
      } else if(socket.room == "school"){
        schoolUsers--;
        numUsers = schoolUsers;
      }
      
      io.to(socket.room).emit('user left', {
        username: socket.username,
        numUsers: numUsers
      });
    }
  });
});
